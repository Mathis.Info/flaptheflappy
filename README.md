# Flap The Flappy 
Le projet est un Flappy Bird.

Dans ce jeu, on peut : 
- Lancer le jeu depuis un menu (avec 1 seul choix dans ce menu). 
- Pendant une partie, on peut mettre le jeu en pause et reprendre en appuyant sur le même boutton. 
- A la fin d'une partie, on peut revenir au menu ou relancer le jeu directement en appuyant sur "OK".

Ce qui a été ajouté : 
- Une fonction de pause en jeu 
- Un son pour chaque appuie du joueur 
- Un son pour chaque point gagné 
- Quand l'oiseau meurt, il tombe en tournoyant 
- Une fois mort, on récupère une médaille (différente en fonction de son score) 
- La medaille apparait en grossissant
-  Le score apparait en défilant (trop vite) Un meilleur score est disponible

Ce qui manque : 
- Différents décors pour varier les partie ---> Je l'ai considérer comme peu important et je l'ai donc délaissé 
- Pas d'items bonus ---> je savais pas trop quoi ajouter 
- L'oiseau s'incline en montant et descendant ---> J'ai pas réussit à l'implémenter 
- Difficultée croissante en jeu ---> plus le jeu avance plus les tuyaux arrivent vite, ou plus la maniabilité de l'oiseau diminiue (+ de gravitée et de force dans l'impulsions de l'oiseau)

Ce qu'il faudrait améliorer : 
- Réduire les Batchs ---> Pour une raison qui m'échappe il sont **démesurément** haut !!! (jusqu'à 10 en jeu !) C'est pour moi le plus gros point noir à l'heure actuelle 
- Meilleur gestion du bouton pause ---> pas toujours évident à cliquer et provoque quelques bugs sur PC 
- Animation mieux visible ---> La médaille et le score ont des animations qu'il faudrait respectivement paufiner et ralentir Intégré tous les items de la section précédente *"Ce qui manque"*

Lors des test sur un Samsung S8, l'image semblait plus grosse donc moins de visibilité pour jouer mais sinon rien de particulier.
