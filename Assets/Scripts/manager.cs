using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cameradatas;

public class manager : MonoBehaviour
{
	public GameObject prefab;
	public int numberOfObjects = 2;
	private Queue<GameObject> objectQueue;
	private Vector2 size;
	private float midScreen;


	void Start(){
		objectQueue = new Queue<GameObject>(numberOfObjects);

		GameObject o = (GameObject) Instantiate(prefab);
		objectQueue.Enqueue(o);

		// ------------------ ModIficationn du manager ---------------------------
		size = objectQueue.Peek().gameObject.GetComponent<Transform>().GetChild(0).gameObject.GetComponent<SpriteRenderer>().bounds.size;
		midScreen = (datas.cameraBorderDotRightTop.x + datas.cameraBorderDotLeftBottom.x)/2;
		// Debug.Log("mid = "+(datas.cameraBorderDotRightTop.x + datas.cameraBorderDotLeftBottom.x)/2);
		// -----------------------------------------------------------------------
	}

	void Update(){

		GameObject myObj = objectQueue.Peek();
		if (myObj != null) {
			if (myObj.GetComponent<Transform>().position.x < datas.cameraBorderDotLeftBottom.x - size.x /2) {
				GameObject o = objectQueue.Dequeue();
				Destroy(o);
			}
			if(myObj.GetComponent<Transform>().position.x < midScreen){
				for (int i = objectQueue.Count; i < numberOfObjects; i++) {
					GameObject o = (GameObject) Instantiate(prefab);
					objectQueue.Enqueue(o);
				}
			}
		}else {
			objectQueue.Dequeue();
		}
	}
}
