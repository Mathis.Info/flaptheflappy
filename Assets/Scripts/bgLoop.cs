﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cameradatas;

public class bgLoop : MonoBehaviour
{
  private Vector2 siz;
  private float speedCoef = 0.7f;
  private float xStart = 11.44f;

  // Start is called before the first frame update
  void Start()
  {
    siz.x	=	gameObject.GetComponent<SpriteRenderer>	().bounds.size.x;
    siz.y	=	gameObject.GetComponent<SpriteRenderer>	().bounds.size.y;
    gameObject.AddComponent<move>().param = new Vector3(-1 ,0,speedCoef);
  }

  void	Update()
  {

  //	If	the	backgound	exits	the	screen
  //	Set	the	X	position	with	the	original	backGround3	X	position
		if	(transform.position.x	<	datas.cameraBorderDotLeftBottom.x	-	(siz.x/2))
		{
			transform.position	=	new	Vector3(xStart,transform.position.y,transform.position.z);
		}
  }
}
