﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using cameradatas;

public class pauseButton : MonoBehaviour
{
    public bool isPaused = false;
    public Camera mainCam;
    // public Camera  pausedCam;

    public void onClick(){
      Debug.Log("Clicked");
      if (isPaused){
        Time.timeScale = 1;
        isPaused = false;
        // mainCam.enable = true;
        // pausedCam.enable = false;

      }else{
        Time.timeScale = 0;
        isPaused = true;
        // mainCam.enable = false;
        // pausedCam.enable = true;

      }
  	}


}
