﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using cameradatas;

public class bird : MonoBehaviour
{

    public AudioClip coinSound;
    public float speedCoef = 5;
    private bool alive;

    void Start()
    {
      alive = true;
    }

    // Update is called once per frame
    void Update()
    {

      // L'oiseau s'envole
      if	(Input.GetKeyDown(KeyCode.Space) && alive)	{
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2( 0 , speedCoef);
        // gameObject.transform.Rotate(0.0f, 0.0f, 45, Space.Self);      //rotate when fly up
        gameState.birdFlaps(); //sound
      }

      // if (Condition) {
      //   gameObject.transform.Rotate(0.0f, 0.0f, 15, Space.Self);
      // }

      //rotation de l'oiseau mort
      if (!alive){
        float newAngle = transform.rotation.z+5;
        if (newAngle > 180) {newAngle = -180+(newAngle-180);}
        gameObject.transform.Rotate(0.0f, 0.0f, newAngle, Space.Self);
      }

      //oiseau sort de l'écran = destroy + final scene
      if (gameObject.transform.position.y < datas.cameraBorderDotLeftBottom.y - gameObject.GetComponent<SpriteRenderer>().bounds.size.y) {
        Destroy(gameObject);
        SceneManager.LoadScene("Scene4-End", LoadSceneMode.Single);
      }

      if (gameObject.transform.position.y > datas.cameraBorderDotLeftTop.y) {
        alive = false;
        Destroy(gameObject.GetComponent<CircleCollider2D>());
      }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
      if(col.gameObject.name == "greenpipe_down" || col.gameObject.name == "greenpipe_up" || col.gameObject.name == "ground"){
        // Debug.Log("PAAAF ! "+ col.gameObject.name + " : " + gameObject.name + " : " + Time.time +" ");
        alive = false;
        Destroy(gameObject.GetComponent<CircleCollider2D>());
      }
      if (col.gameObject.name == "coin") {
        gameState.addScorePlayer(1);
        gameState._instance.MakeSound(coinSound);
      }

    }

}
