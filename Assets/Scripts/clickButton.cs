﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class clickButton : MonoBehaviour
{
      private bool isPaused = false;
      public Camera mainCam;

    public void onClick(){
      string buttonName = this.gameObject.tag;
      // Debug.Log("name : "+buttonName);
      switch(buttonName){
        case "GameLaunch": GoScene3();
          break;
        case "Menu" :GoScene2();
          break;
        case "OnPause": Pause();
          break;
        default : Wait2sec();
          break;
      }
    }


    void	Wait2sec()	{
  					Invoke	("GoScene2",2.0f);
  	}
  	void	GoScene2(){
  					 //	Applica;on.LoadLevel("Scene2-Menu");	//	Obsolète depuis	la	version	4…
             SceneManager.LoadScene("Scene2-Menu");
  	}
    void	GoScene3(){
  					 //	Applica;on.LoadLevel("Scene2-Menu");	//	Obsolète depuis	la	version	4…
             SceneManager.LoadScene("Scene3-Game",LoadSceneMode.Single);
  	}

    // public Camera  pausedCam;

    void Pause(){
      if (isPaused){
        Time.timeScale = 1;
        isPaused = false;
        // mainCam.enable = true;
        // pausedCam.enable = false;

      }else{
        Time.timeScale = 0;
        isPaused = true;
        // mainCam.enable = false;
        // pausedCam.enable = true;

      }
    }
}


    // Use this for initialization
    // void Start()
    // {
    //     myLoadedAssetBundle = AssetBundle.LoadFromFile("Assets/AssetBundles/scenes");
    //     scenePaths = myLoadedAssetBundle.GetAllScenePaths();
    // }
    //
    // void OnGUI()
    // {
    //     if (GUI.Button(new Rect(10, 10, 100, 30), "Change Scene"))
    //     {
    //         Debug.Log("Scene2 loading: " + scenePaths[0]);
    //         SceneManager.LoadScene(scenePaths[0], LoadSceneMode.Single);
    //     }
    // }
