﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;




public class gameState : MonoBehaviour
{
  public static gameState _instance;

  void Awake(){
    if (_instance == null){
      _instance = this;
      DontDestroyOnLoad(_instance.gameObject);
    }
    else if (this != _instance) Destroy(this);
  }

  private static int bestScore = 0;
  private static int scorePlayer = 0;
  private static string scoreSt;
  public AudioClip audioClip;


  // Start is called before the first frame update
  void Start()
  {
    // GameObject.FindWithTag("scoreLabel").GetComponent<Text>().text	=	"0";
  }

  public static void addScorePlayer(int toAdd)	{
    scorePlayer	+=	toAdd;
    GameObject label = GameObject.FindWithTag("scoreLabel");
    if (label != null) {
      label.GetComponent<Text>().text	= scoreSt + scorePlayer.ToString();
    }
  }

  public static int getScorePlayer(){
    return	scorePlayer;
  }
  public static void resetScorePlayer(){
    scorePlayer = 0;
  }

  public static int getBestScore(){
    return	bestScore;
  }

  public static bool isBestScore(int newScore){
    bool b = newScore > bestScore;
    if (b)  bestScore = newScore;
    return b;
  }


  public static void birdFlaps(){
    _instance.MakeSound(_instance.audioClip);
  }

  public void MakeSound(AudioClip audioClip){
    AudioSource.PlayClipAtPoint(audioClip,transform.position);
  }
}
