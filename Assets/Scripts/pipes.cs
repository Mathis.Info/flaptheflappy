﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cameradatas;

public class pipes : MonoBehaviour
{
    private Vector3 initialPos;
    private Vector2 sizePipe;
    public float speedCoef = 1f;

    // Start is called before the first frame update
    void Start()
    {
      sizePipe = gameObject.GetComponent<Transform>().GetChild(0).gameObject.GetComponent<SpriteRenderer>().bounds.size;

      initialPos = new Vector3(
                  datas.cameraBorderDotRightTop.x+(sizePipe.x/2),
                  Random.Range(-1,4),
                  0);

      gameObject.GetComponent<Transform>().position = initialPos;
      gameObject.AddComponent<move>().param = new Vector3(-1 ,0,speedCoef);

    }
}
