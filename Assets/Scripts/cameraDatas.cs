using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace cameradatas{
  public class datas
  {
      public static Vector3 cameraBorderDotLeftBottom = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
      public static Vector3 cameraBorderDotRightBottom = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
      public static Vector3 cameraBorderDotLeftTop = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
  	  public static Vector3 cameraBorderDotRightTop = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
  }
}
