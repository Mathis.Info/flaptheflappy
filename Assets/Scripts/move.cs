using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{
    private Vector2 movement;
    private float axeX;
    private float axeY;
    private float speed;
    public Vector3 param;


    // Start is called before the first frame update
    void Start()
    {
      axeX = param.x;
      axeY = param.y;
      speed = param.z;
    }

    // Update is called once per frame
    void Update()
    {
    movement = new Vector2( axeX * speed , axeY * speed);

    gameObject.GetComponent<Rigidbody2D>().velocity = movement;
    }
}
