﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// effet sur la medaille : elle grossit puis s'ajuste au panneau
public class impulseEffect : MonoBehaviour
{
    private Vector3 scale;
    public  Vector2 param;
    private float maxScale;
    private float finalScale;
    private float speed = 0.01f;

    // Start is called before the first frame update
    void Start()
    {
      finalScale = param.x;
      speed      = param.y;
      maxScale   = finalScale*1.2f;
      GetComponent<Transform>().localScale = new Vector3(0,0,1);
    }

    // Update is called once per frame
    void Update()
    {
      Vector3 scaleChange = new Vector3( 0, 0, 0);

      scale = GetComponent<Transform>().localScale ;
      if (scale.x < maxScale ) {
        scaleChange.x = speed;
        scaleChange.y = speed;
        GetComponent<Transform>().localScale += scaleChange;
      }
      else{
        StartCoroutine(waitSecond());
        GetComponent<Transform>().localScale = new Vector3(finalScale,finalScale,1);
        Destroy(GetComponent<impulseEffect>());
      }
    }

    IEnumerator waitSecond (){
      // Debug.Log("WAIT");
        yield return new WaitForSeconds(3);
      }
}
