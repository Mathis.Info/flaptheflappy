﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class finalPanel : MonoBehaviour
{
    public Sprite[] spriteArray;
    public Sprite gold;
    public Sprite silver;
    public Sprite bronze;
    private int mostScore;
    private int scorePlayer;
    private int score;
    // Start is called before the first frame update
    void Start()
    {
      scorePlayer = gameState.getScorePlayer();
      bool isNewPB = gameState.isBestScore(scorePlayer);
      if (isNewPB) {
        //Add text : "New Personal Best"
      }
      mostScore = gameState.getBestScore();
      gameState.resetScorePlayer();

      GameObject g = GameObject.Find("Final Score/Most Score");

      for (int i = 0; i<3 ; i++) {

        GameObject child = g.transform.GetChild(i).gameObject;
        int numSprite = mostScore%10;
        // if (numSprite==0) child.GetComponent<SpriteRenderer>().sprite = null;
        child.GetComponent<SpriteRenderer>().sprite = spriteArray[numSprite] ;
        mostScore = mostScore/10;
        }

    }

    // Update is called once per frame
    void Update()
    {
      if (score<=scorePlayer) { //Affichage du score avec effet de score qui défile
        GameObject g = GameObject.FindGameObjectWithTag("unit");
        g.GetComponent<SpriteRenderer>().sprite = spriteArray[score%10] ;
        if (score >= 100) {
          g = GameObject.FindGameObjectWithTag("hundred");
          g.GetComponent<SpriteRenderer>().sprite = spriteArray[(score/100)%10] ;
        }
        if (score >= 10) {
          g = GameObject.FindGameObjectWithTag("digit");
          g.GetComponent<SpriteRenderer>().sprite = spriteArray[(score/10)%10] ;
        }
        score++;

      // Gestion de la médaille
      g = GameObject.FindGameObjectWithTag("medal");
      if(scorePlayer > 10) g.GetComponent<SpriteRenderer>().sprite = gold;
      else if (scorePlayer > 5) g.GetComponent<SpriteRenderer>().sprite = silver;
      else if (scorePlayer == 0) g.GetComponent<SpriteRenderer>().sprite = null;
      else g.GetComponent<SpriteRenderer>().sprite = bronze;
    }
  }
}
